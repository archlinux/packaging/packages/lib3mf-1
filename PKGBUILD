# Maintainer:
# Contributor: Jonas Witschel <diabonas@archlinux.org>

_name=lib3mf
pkgname=lib3mf-1
pkgver=1.8.1
pkgrel=7
pkgdesc='Implementation of the 3D Manufacturing Format file standard (legacy version)'
arch=(x86_64)
url="https://github.com/3MFConsortium/lib3mf"
license=(BSD-2-Clause)
depends=(
  gcc-libs
  glibc
  libzip
  util-linux-libs
  zlib
)
makedepends=(cmake)
checkdepends=(gtest)
options=(!lto)
source=(
  $pkgname-$pkgver.tar.gz::$url/archive/refs/tags/v$pkgver.tar.gz
  $_name-1.8.1-use-system-gtest.patch::$url/commit/cf679cc3736d4b22ce5711b5e81d28b7b6f4f4b0.patch
  $_name-1.8.1-link-libzip-and-zlib.patch::$url/commit/093f2d95045dac25844936a72d5d8134a6265b20.patch
  $_name-1.8.1-CVE-2021-21772.patch::$url/commit/6ac5f521f0a3e9f100814f515e380859c9a6ec46.patch
)
sha512sums=('f7fd75bcb6472de1595a018e5add516d0d78ab0aee92462b686b77f8d2bef05270e7b737cb7e1d31fc5850815056e753874c2f9ec456a455e8461c4010fe914a'
            'fa383aaa4e7c24550b3d773cde42f477f3d1e276a846d1221b34b3e4a88ddee5bc1e17f7f0a69c1521892bda3ce953f0d09bfedfa624689992ca3c24cf4d3d43'
            '8df1824f1ee34d502945d469d20745946d9b1e6216abe8cfe57b75fc06fd5e4a4197456c8334798c0c1a350ec233f75ee7373a0e2869aac490075f116dd8374b'
            '8749dadef328cab986a9bbfc2e28415c3ac48c90449c66db2cb5ddacc9fb0c56208222876c1a728911b880fed5b2cb48f57fadbbc30c4e75c0cfc5f46d66a8b1')

prepare() {
  cd $_name-$pkgver
  patch --strip=1 --input=../$_name-1.8.1-use-system-gtest.patch
  patch --strip=1 --input=../$_name-1.8.1-link-libzip-and-zlib.patch
  patch --strip=1 --no-backup-if-mismatch --input=../$_name-1.8.1-CVE-2021-21772.patch
}

build() {
  local cmake_options=(
    -B build
    -D CMAKE_BUILD_TYPE=None
    -D CMAKE_INSTALL_INCLUDEDIR=include/$pkgname
    -D CMAKE_INSTALL_PREFIX=/usr
    -D LIB3MF_TESTS=OFF  # NOTE: gtests needs C++14 and with it tests fail
    -D USE_INCLUDED_GTEST=OFF
    -D USE_INCLUDED_LIBZIP=OFF
    -D USE_INCLUDED_ZLIB=OFF
    -S $_name-$pkgver
    -W no-dev
  )

  cmake "${cmake_options[@]}"
  cmake --build build
}

check() {
  ctest --test-dir build --output-on-failure
}

package() {
  DESTDIR="$pkgdir" cmake --install build
  install -Dm644 $_name-$pkgver/LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}
